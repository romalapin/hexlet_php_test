FROM hexlet/hexlet-vagrant

# All our dependencies, in alphabetical order (to ease maintenance)
RUN apt-get update && apt-get install -y --no-install-recommends \
        curl \
        git \
        php5-cli \
        php5-curl \
        php5-gd \
        php5-imagick \
        php5-intl \
        php5-json \
        php5-ldap \
        php5-mcrypt \
        php5-mhash \
        php5-mysql \
        php5-pgsql \
        php5-sqlite

# Install composer
RUN curl -sS http://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install phpunit
RUN apt-get install -y -f phpunit

# CMD php -v

# ADD . /usr/src/app
