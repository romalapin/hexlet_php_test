# Simplest "Hello, world"

Complete function in the way it returns the string "Hello, " with attached name at the end. For example, if `$name = 'Jack'` function should return "Hello, Jack".