<?php
require_once 'hello.php';

class test extends PHPUnit_Framework_TestCase {

    public function testHello() {
        $this->assertEquals(hello('Jack'), 'Hello, Jack');
    }

}